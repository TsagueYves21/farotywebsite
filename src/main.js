import { createApp } from 'vue'
import { createI18n } from 'vue-i18n'

import 'jquery'
import 'popper.js'
import 'bootstrap'

import App from './App.vue'
import router from './router'
import store from './store'

import './assets/include-bootrap.css'
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'

import fr from './locales/fr.json'
import en from './locales/en.json'

const i18n = createI18n({
  locale: 'en',
  messages: {
    en,
    fr,
  },
})

createApp(App).use(store).use(router).use(i18n).mount('#app')
